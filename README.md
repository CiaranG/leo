
# Overview

An Android verbal comminication aid.

This is very much a test/experimental piece of software, which you are welcome
to use, but expect it to be possibly buggy, definitely not user-friendly in
some areas, and subject to huge changes at any moment (or no changes for ages!).

The idea is to be able to quickly construct spoken sentences through pressing
icons (although there is also the option to type when necessary). With the
correct 'layout' (not the current default) this can be quite effective, but
you will need to do a lot of editing from the current default to get something
useful.

The speech engine used is whatever you have selected via the standard Android
Settings, so you can change it there.

There are two main modes - editable or not. You can turn editing on and off
in Preferences. A 'layout' is loadable and savable, and basically consists
of a hierarchical tree of screen/icon layouts, within which you can reference
folders from elsewhere, create follow-on links, etc. This is the means by
which you define the available grammar, etc, such that sentence construction
presents you with relevant options as you proceed. If this sounds complicated,
it probably is, and I am not going to document it any further at this stage
because I might change it!

Having said all the above, if you still insist on trying it, you can get it
from my F-Droid repo which is at http://ciarang.com/repo, or you can build
it yourself.

# Developers

Don't expect to find any elegant design or code here, it's mostly experimental
and work in progress!

# Icon Licenses

Some of the built-in icons are CC licensed, and created by Sergio Palao for
CATEDU. Visit http://arasaac.org/ for more information.


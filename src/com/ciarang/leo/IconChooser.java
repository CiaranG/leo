/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.ciarang.leo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class IconChooser extends Activity {

    private GridView mGridView;
    private IconAdapter mIconAdapter;

    private ProgressDialog mProgressDialog = null;
    private ArrayList<SearchResult> mSearchResults = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        readPrefs();

        setContentView(R.layout.iconchooser);
        setTitle(R.string.iconchooser_title);

        mGridView = (GridView) findViewById(R.id.gridview);
        mIconAdapter = new IconAdapter(this);
        mGridView.setAdapter(mIconAdapter);

        Intent i = getIntent();
        String searchtext = i.getStringExtra("searchtext");
        String type = i.getStringExtra("type");

        if (type.equals("internal")) {
            for (LeoIcon.Internals internal : LeoIcon.Internals.values()) {
                try {
                    mIconAdapter.Icons.add(new LeoIcon(internal));
                } catch (Exception e) {
                }
            }
            mIconAdapter.notifyDataSetChanged();
        } else if (type.equals("library") || type.equals("online")) {
            search(searchtext, type);
        } else {
            finish();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private Handler update_handler;

    public void search(String text, String type) {

        final String key = text;
        final String ftype = type;

        mProgressDialog = ProgressDialog.show(this,
                getString(R.string.searching_title),
                getString(R.string.searching_msg), true);
        mProgressDialog.setIcon(android.R.drawable.ic_dialog_info);

        update_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    Toast.makeText(IconChooser.this,
                            getString(R.string.search_error_msg),
                            Toast.LENGTH_LONG).show();
                } else {
                    mIconAdapter.Icons.clear();
                    for (SearchResult s : mSearchResults) {
                        try {
                            if (s.LibPath != null)
                                mIconAdapter.Icons.add(new LeoIcon(s.LibPath));
                            else
                                mIconAdapter.Icons
                                        .add(new LeoIcon(s.Thumbnail));
                        } catch (Exception e) {
                        }
                    }
                    mIconAdapter.notifyDataSetChanged();
                }
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        };

        new Thread() {
            public void run() {
                if (ftype.equals("library")) {
                    File lib = new File(getExternalFilesDir(null),
                            "arasaac_col_small.jar");

                    mSearchResults = doSearchJar(lib, key);
                } else {
                    mSearchResults = doSearchOcal(key);
                }
                update_handler.sendEmptyMessage((mSearchResults == null) ? 0
                        : 1);
            }
        }.start();

    }

    public void cancel(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }

    private class IconAdapter extends BaseAdapter {
        private Context mContext;
        public ArrayList<LeoIcon> Icons = new ArrayList<LeoIcon>();

        public IconAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return Icons.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // Create a new item...
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) { // if it's not recycled, initialize some
                                       // attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent data = new Intent();
                    LeoIcon icon = (LeoIcon) v.getTag();
                    try {
                        data.putExtra("icon", icon.Encoded());
                    } catch (Exception e) {
                    }
                    setResult(RESULT_OK, data);
                    finish();
                }
            });
            try {
                imageView.setImageBitmap(Icons.get(position)
                        .getBitmap(mContext));
            } catch (Exception e) {
            }
            imageView.setTag(Icons.get(position));

            return imageView;
        }
    }

    private class SearchResult {
        public String Title;
        public String LibPath;
        public Bitmap Thumbnail;
    }

    private ArrayList<SearchResult> doSearchOcal(String key) {

        try {
            String url;
            url = "http://openclipart.org/media/feed/rss/"
                    + URLEncoder.encode(key, "UTF-8");
            String xml = getHTTP(url);

            // Temporary workaround for
            // https://bugs.launchpad.net/openclipart/+bug/713333
            xml = xml.replace("&", "&amp;");

            ArrayList<SearchResult> results = new ArrayList<SearchResult>();
            SearchResult curresult = null;

            StringReader reader = new StringReader(xml);
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(reader);
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (parser.getName().equals("item")) {
                        curresult = new SearchResult();
                        curresult.LibPath = null;
                    } else if (parser.getName().equals("title")) {
                        if (curresult != null) {
                            parser.next();
                            curresult.Title = parser.getText();
                        }
                    } else if (parser.getName().equals("thumbnail")) {
                        url = parser.getAttributeValue("", "url");
                        // openclipart.org returns invalid URLs
                        url = url.replace(" ", "%20");
                        curresult.Thumbnail = getHTTPBitmap(url);
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (parser.getName().equals("item")) {
                        if (curresult != null) {
                            results.add(curresult);
                            curresult = null;
                        }
                    }
                    break;
                }
                eventType = parser.next();
            }
            return results;

        } catch (Exception ex) {
            Log.d("Leo", "Search failed - " + ex.getMessage());
            Log.v("Leo", Log.getStackTraceString(ex));
            return null;
        }

    }

    private ArrayList<SearchResult> doSearchJar(File jar, String key) {

        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        SearchResult curresult = null;
        try {
            if (!jar.exists())
                return null;

            JarFile jf = new JarFile(jar);
            Enumeration<JarEntry> entries = jf.entries();
            while (entries.hasMoreElements()) {
                JarEntry je = entries.nextElement();
                String f = je.getName();
                if (f.endsWith(".png")
                        && f.toLowerCase().contains(key.toLowerCase())) {
                    curresult = new SearchResult();
                    curresult.Title = f.substring(0, f.length() - 4);
                    curresult.LibPath = "A:" + f;
                    InputStream is = jf.getInputStream(je);
                    Bitmap b = BitmapFactory.decodeStream(is);
                    is.close();
                    curresult.Thumbnail = Bitmap.createScaledBitmap(b, 85, 85,
                            false);
                    b.recycle();
                    results.add(curresult);
                }
            }
            jf.close();
            return results;

        } catch (Exception ex) {
            Log.d("Leo", "Search failed - " + ex.getMessage());
            Log.v("Leo", Log.getStackTraceString(ex));
            return null;
        }

    }

    private ArrayList<SearchResult> doSearchDir(File dir, String key) {

        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        SearchResult curresult = null;
        try {
            if (!dir.exists())
                return null;

            for (String f : dir.list()) {
                if (f.endsWith(".png")
                        && f.toLowerCase().contains(key.toLowerCase())) {
                    curresult = new SearchResult();
                    curresult.Title = f.substring(0, f.length() - 4);
                    curresult.LibPath = "ARASAAC/" + f;
                    Bitmap b = BitmapFactory.decodeFile(new File(dir, f)
                            .getPath());
                    curresult.Thumbnail = Bitmap.createScaledBitmap(b, 85, 85,
                            false);
                    b.recycle();
                    results.add(curresult);
                }
            }

            return results;

        } catch (Exception ex) {
            Log.d("Leo", "Search failed - " + ex.getMessage());
            Log.v("Leo", Log.getStackTraceString(ex));
            return null;
        }

    }

    private Bitmap getHTTPBitmap(String url) throws MalformedURLException,
            IOException, URISyntaxException {
        URLConnection connection = new URI(url).toURL().openConnection();
        connection.connect();
        InputStream is = connection.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is, 1024 * 8);
        Bitmap b = BitmapFactory.decodeStream(bis);
        bis.close();
        is.close();
        return b;
    }

    private String getHTTP(String url) throws URISyntaxException,
            ClientProtocolException, IOException {

        String response = null;
        DefaultHttpClient http = new DefaultHttpClient();
        HttpGet httpMethod = new HttpGet();
        httpMethod.setURI(new URI(url));
        HttpResponse res = http.execute(httpMethod);
        if (res.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = res.getEntity();
            if (entity != null)
                response = EntityUtils.toString(entity);
        }
        return response;
    }

    // Read frequently accessed preferences into local members.
    private void readPrefs() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        if (prefs.getBoolean("fullscreen", false))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}

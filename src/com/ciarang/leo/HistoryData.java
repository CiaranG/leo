package com.ciarang.leo;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;

public class HistoryData {

    // This is a list of history items. The most recent is at the
    // end!
    public ArrayList<String> Data;
    
    public HistoryData() {
        Data = new ArrayList<String>();
    }

    public void add(String item) {
        Data.remove(item);
        Data.add(item);
        while(Data.size() > 50)
            Data.remove(0);
    }

    public static HistoryData fromXml(String xml) throws Exception {
        HistoryData h = new HistoryData();
        StringReader reader = new StringReader(xml);
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(reader);
        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
            case XmlPullParser.START_TAG:
                String name = parser.getName();
                if (name.equals("historyitem")) {
                    parser.next();
                    h.add(parser.getText());
                }
                break;
            }
            eventType = parser.next();
        }
        return h;
    }

    public String toXml() throws IllegalArgumentException,
            IllegalStateException, IOException {
        XmlSerializer s = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        s.setOutput(writer);
        s.startDocument("UTF-8", true);
        s.startTag("", "historydata");
        s.attribute("", "version", String.valueOf(1));
        for(String item : Data) {
            s.startTag("", "historyitem");
            s.text(item);
            s.endTag("", "historyitem");
        }
        s.endTag("", "historydata");
        s.endDocument();
        return writer.toString();
    }
    
    
}

/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.ciarang.leo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public abstract class Item {
    public String Caption;

    // Text to speak for this item. Sometimes it's an empty string or
    // null, e.g. for groups that just lead to more items.
    public String Speech;

    // ID of group to go to after this, once a Speech item with no Goto
    // has been selected.
    public String After;

    // The icon for this item.
    public LeoIcon Icon;

    // The parent group for this item.
    public GroupItem Parent;

    public enum Flag {

        // Any item with this present sets the whole current sentence to be
        // a question...
        question,

        // Set on a place to signify that it's the kind of 'special place'
        // that requires a "to". Compare "go TO the park", "go home", etc.
        toplace,
    }

    public List<Flag> Flags;

    // Returns true if the item has the specified flag, false otherwise.
    public boolean hasFlag(Flag flag) {
        return (Flags.contains(flag));
    }

    // A unique ID, used for references.
    public String ID;

    public abstract void ToXML(XmlSerializer s)
            throws IllegalArgumentException, IllegalStateException, IOException;

    public Item() {
        ID = UUID.randomUUID().toString();
        After = null;
        Speech = null;
        Flags = new ArrayList<Flag>();
    }

    // Examples of paths...
    //
    // "" - the root group itself
    // "4" - item 4 in the root group
    // "+g" - new group item in root group
    // "4/+s" - new speech item in group 4 within the root group
    // "4/2" - item 2 in the group 4 within the root group
    //
    // (the +s and +g variants aren't returned by this, but are used
    // elsewhere)
    //
    public String getPath() {
        String path = "";
        GroupItem i = Parent;
        Item t = this;
        while (i != null) {
            int index = i.Items.indexOf(t);
            if (path.length() > 0)
                path = "/" + path;
            path = Integer.toString(index) + path;
            t = i;
            i = t.Parent;
        }
        return path;
    }

    protected static boolean handleCommonTag(String name, Item g,
            XmlPullParser parser) throws Exception {

        if (name.equals("caption")) {
            parser.next();
            g.Caption = parser.getText();
            return true;
        } else if (name.equals("flags")) {
            parser.next();
            for (String s : parser.getText().split(","))
                g.Flags.add(Flag.valueOf(s));
            return true;
        } else if (name.equals("id")) {
            parser.next();
            g.ID = parser.getText();
            return true;
        } else if (name.equals("icon")) {
            parser.next();
            g.Icon = LeoIcon.Decode(parser.getText());
            return true;
        } else if (name.equals("after")) {
            parser.next();
            g.After = parser.getText();
            return true;
        } else if (name.equals("speech")) {
            parser.next();
            g.Speech = parser.getText();
        }
        return false;
    }

    protected void CommonToXML(XmlSerializer s)
            throws IllegalArgumentException, IllegalStateException, IOException {
        s.startTag("", "id");
        s.text(ID);
        s.endTag("", "id");
        if (Caption != null) {
            s.startTag("", "caption");
            s.text(Caption);
            s.endTag("", "caption");
        }
        if (Flags.size() > 0) {
            s.startTag("", "flags");
            boolean first = true;
            for (Flag f : Flags) {
                if (first)
                    first = false;
                else
                    s.text(",");
                s.text(f.toString());
            }
            s.endTag("", "flags");
        }
        if (After != null) {
            s.startTag("", "after");
            s.text(After);
            s.endTag("", "after");
        }
        s.startTag("", "icon");
        s.text(Icon.Encoded());
        s.endTag("", "icon");
        if (Speech != null && Speech.length() > 0) {
            s.startTag("", "speech");
            s.text(Speech);
            s.endTag("", "speech");
        }
    }

    @Override
    public String toString() {
        // Overriding this to return the caption, so we can use it directly
        // in an ArrayAdapter (for now, at least).
        return Caption;
    }

}

/*
 * Copyright (C) 2010-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.ciarang.leo;

import com.ciarang.leo.Item.Flag;
import com.ciarang.leo.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.*;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.text.InputType;
import android.util.Log;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.speech.tts.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

public class Leo extends Activity implements TextToSpeech.OnInitListener {

    private static final int HISTORY = Menu.FIRST;
    private static final int LAYOUTS = Menu.FIRST + 1;
    private static final int PREFERENCES = Menu.FIRST + 2;
    private static final int ABOUT = Menu.FIRST + 3;

    private static final int REQUEST_PREFS = 0;
    private static final int REQUEST_ITEMEDIT = 1;
    private static final int REQUEST_LAYOUTS = 2;
    private static final int REQUEST_HISTORY = 3;
    private static final int REQUEST_MOVETO = 4;

    private TextToSpeech mTTS;

    // The current board layout. This is always persisted as default.xml in
    // local storage, but we keep track of where it came from originally and
    // whether it has been modified.
    private BoardLayout mLayout;

    private GridView mGridView;
    private ImageAdapter mImageAdapter;

    private HistoryData mHistory;

    private boolean mEditable = true;
    private boolean mFullScreen = false;

    // The sentence currently being built, as a list of the items which
    // have been chosen.
    private List<Item> mSentence;;

    private ProgressDialog mProgress;
    private Handler mHandler;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        mHandler = new Handler();

        mGridView = (GridView) findViewById(R.id.gridview);
        registerForContextMenu(mGridView);
        mImageAdapter = new ImageAdapter(this);

        mTTS = new TextToSpeech(this, this);

        readPrefs();

        ImageButton backbutton = (ImageButton) findViewById(R.id.btnBack);
        backbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeLastSpeech();
            }
        });
        backbutton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                backToTop();
                return true;
            }
        });

        TextView tv = (TextView) findViewById(R.id.text);
        tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                if (tv.getInputType() == InputType.TYPE_NULL) {
                    tv.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                }
            }
        });

        // Set the audio stream type that the hardware volume buttons
        // will affect...
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        mProgress = ProgressDialog.show(this, "Loading",
                "Loading, please wait..");
        Thread t = new Thread() {
            public void run() {
                Init();
                mHandler.post(new Runnable() {
                    public void run() {
                        mProgress.dismiss();
                        mImageAdapter.setGroup(mLayout.RootItem);
                        mGridView.setAdapter(mImageAdapter);
                        backToTop();
                    };
                });
            }
        };
        t.start();

    }

    private void Init() {

        // Try and load the default saved board layout, and if that's not
        // possible just generate a default in code...
        try {
            FileInputStream f = openFileInput("default.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(f,
                    "UTF-8"));
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = br.readLine()) != null)
                builder.append(line);
            br.close();
            f.close();
            mLayout = BoardLayout.fromXml(builder.toString());
            Log.d("Leo", "Loaded default.xml");

            mLayout.PreloadIcons(this);

        } catch (Exception e) {
            Log.d("Leo",
                    "Creating default layout, because it couldn't be loaded - "
                            + e.getMessage());
            mLayout = getDefaultLayout();
            Log.d("Leo", "Used generated default layout.");
        }

        // Load saved history if it exists - otherwise start with a blank
        // one.
        try {
            FileInputStream f = openFileInput("history.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(f,
                    "UTF-8"));
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = br.readLine()) != null)
                builder.append(line);
            br.close();
            f.close();
            mHistory = HistoryData.fromXml(builder.toString());
        } catch (Exception e) {
            mHistory = new HistoryData();
        }

    }

    @Override
    protected void onPause() {

        // Save the current layout. Note that we're trusting it to actually
        // get here, otherwise you lose your changes. We could even do this
        // in OnDestroy() but that seems even more dangerous. An obvious
        // data loss scenario is if you pull your battery. Still, this seems
        // preferable to saving after every change - perhaps?
        try {
            FileOutputStream f = openFileOutput("default.xml",
                    Context.MODE_PRIVATE);
            f.write(mLayout.toXml().getBytes());
            f.close();
            Log.d("Leo", "Saved current layout");
        } catch (Exception ex) {
            Log.d("Leo", "Failed to save layout - " + ex.getMessage());
        }

        // Save history.
        try {
            FileOutputStream hf = openFileOutput("history.xml",
                    Context.MODE_PRIVATE);
            hf.write(mHistory.toXml().getBytes());
            hf.close();
        } catch (Exception ex) {
            Log.d("Leo", "Failed to save history - " + ex.getMessage());
        }

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mTTS != null) {
            mTTS.shutdown();
            mTTS = null;
        }

        super.onDestroy();
    }

    public void onInit(int version) {
        // TODO: Show 'ready' status here somehow, because it can
        // take time for the TTS system to initialise.
    }

    public void backToTop() {
        TextView tv = (TextView) findViewById(R.id.text);
        tv.setInputType(InputType.TYPE_NULL);
        tv.setText("");
        mSentence = new ArrayList<Item>();
        mImageAdapter.setGroup(mLayout.RootItem);
    }

    // Speak the current sentence.
    public void speak() {
        speak(null);
    }

    public void speak(View v) {
        TextView tv = (TextView) findViewById(R.id.text);
        String text = tv.getText().toString();
        HashMap<String, String> parms = new HashMap<String, String>();
        parms.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, text);
        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, parms);
        mHistory.add(text);
        backToTop();
    }

    /**
     * Launch the ItemEdit Activity to edit an Item.
     * 
     * @param item
     *            - The Item to be edited.
     */
    private void editItem(Item item) {
        Intent data = new Intent(getBaseContext(), ItemEdit.class);
        try {
            data.putExtra("layout", mLayout.toXml());
        } catch (Exception e) {
            Log.d("Leo",
                    "Failed to read generate layout xml - " + e.getMessage());
            Log.v("Leo", Log.getStackTraceString(e));
            return;
        }
        if (item.Parent != null)
            data.putExtra("returnpath", item.Parent.getPath());
        data.putExtra("path", item.getPath());
        startActivityForResult(data, REQUEST_ITEMEDIT);
    }

    /**
     * Launch the ItemEdit Activity to create a new Item.
     * 
     * @param group
     *            - The parent group for the new Item.
     * @param position
     *            - The item number to insert at
     * @param newgroup
     *            - True to create a group, False for a normal speech item.
     */
    private void newItem(GroupItem group, int position, boolean newgroup) {
        Intent data = new Intent(getBaseContext(), ItemEdit.class);
        try {
            data.putExtra("layout", mLayout.toXml());
        } catch (Exception e) {
            Log.d("Leo",
                    "Failed to read generate layout xml - " + e.getMessage());
            Log.v("Leo", Log.getStackTraceString(e));
            return;
        }
        String path = group.getPath();
        data.putExtra("returnpath", path);
        if (path.length() > 0)
            path += '/';
        if (newgroup)
            path += "+g" + position;
        else
            path += "+s" + position;
        data.putExtra("path", path);
        startActivityForResult(data, REQUEST_ITEMEDIT);
    }

    private static final int CONTEXT_OPEN = 0;
    private static final int CONTEXT_USE = 1;
    private static final int CONTEXT_SAY = 2;
    private static final int CONTEXT_EDIT = 3;
    private static final int CONTEXT_INSERTSPEECH = 4;
    private static final int CONTEXT_INSERTGROUP = 5;
    private static final int CONTEXT_MOVETO = 6;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.gridview) {
            AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
            GroupItem grp = mImageAdapter.getGroup();

            // Get the item being referred to. If it's beyond list of items in
            // the group, we must be in edit mode, and it's the 'fake' item.
            // We'll use null for that.
            Item item;
            if (info.position == grp.allItems().size())
                item = null;
            else
                item = grp.allItems().get(info.position);
            boolean isincluded = item != null
                    && info.position >= grp.Items.size();

            // Now we can decide what context menu options are available...
            if (item != null && item.getClass() == GroupItem.class) {
                menu.add(Menu.NONE, CONTEXT_OPEN, 0, R.string.context_open);
            }
            if (item != null && item.getClass() == SpeechItem.class) {
                menu.add(Menu.NONE, CONTEXT_USE, 0, R.string.context_use);
                menu.add(Menu.NONE, CONTEXT_SAY, 1, R.string.context_say);
            }
            if (mEditable) {
                if (item != null)
                    menu.add(Menu.NONE, CONTEXT_EDIT, 2, R.string.context_edit);
                menu.add(Menu.NONE, CONTEXT_INSERTSPEECH, 3,
                        R.string.context_insertspeech);
                menu.add(Menu.NONE, CONTEXT_INSERTGROUP, 4,
                        R.string.context_insertgroup);
                if (!isincluded)
                    menu.add(Menu.NONE, CONTEXT_MOVETO, 5,
                            R.string.context_moveto);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
                .getMenuInfo();
        GroupItem grp = mImageAdapter.getGroup();

        // Get the item being referred to. If it's beyond list of items in
        // the group, we must be in edit mode, and it's the 'fake' item.
        // We'll use null for that.
        // TODO: Same as in onCreateContextMenu!!
        Item citem;
        if (info.position == grp.allItems().size())
            citem = null;
        else
            citem = grp.allItems().get(info.position);

        switch (item.getItemId()) {
        case CONTEXT_OPEN:
            mImageAdapter.setGroup((GroupItem) citem);
            break;
        case CONTEXT_USE:
            useSpeech((SpeechItem) citem);
            break;
        case CONTEXT_SAY:
            saySpeech((SpeechItem) citem);
            break;
        case CONTEXT_EDIT:
            editItem(citem);
            break;
        case CONTEXT_INSERTGROUP:

            // Need to constrain insert position, because there might be
            // included items.
            int inspos = info.position;
            int max = mImageAdapter.getGroup().Items.size() - 1;
            if (inspos > max)
                inspos = max;

            newItem(grp, inspos, true);
            break;

        case CONTEXT_INSERTSPEECH:

            // Need to constrain insert position, because there might be
            // included items.
            int inspos2 = info.position;
            int max2 = mImageAdapter.getGroup().Items.size() - 1;
            if (inspos2 > max2)
                inspos2 = max2;

            newItem(grp, inspos2, false);
            break;
        case CONTEXT_MOVETO:
            Intent i = new Intent(getBaseContext(), ItemChooser.class);
            try {
                i.putExtra("layout", mLayout.toXml());
                i.putExtra("donotshow", mImageAdapter.getGroup().ID);
                i.putExtra("title", R.string.movetowhere);
                i.putExtra("extra", citem.ID);
                startActivityForResult(i, REQUEST_MOVETO);
            } catch (Exception e) {

            }
            break;
        }
        return true;

    }

    // Say a single item.
    private void saySpeech(SpeechItem item) {
        HashMap<String, String> parms = new HashMap<String, String>();
        parms.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, item.Speech);
        mTTS.speak(item.Speech, TextToSpeech.QUEUE_FLUSH, parms);
    }

    // Add the given item to the sentence currently being built.
    private void useSpeech(Item item) {
        mSentence.add(item);
        updateSentenceText();
    }

    // Remove the last item from the sentence currently being built.
    // i.e. when using the back button
    private void removeLastSpeech() {
        Item removed = null;
        while (mSentence.size() > 0) {
            removed = mSentence.get(mSentence.size() - 1);
            mSentence.remove(mSentence.size() - 1);
            if (removed.Speech != null)
                break;
        }
        updateSentenceText();
        if(removed == null) {
            mImageAdapter.setGroup(mLayout.RootItem);            
        } else {
            mImageAdapter.setGroup(removed.Parent);
        }
    }

    // Update the text to reflect the sentence currently being built.
    private void updateSentenceText() {
        String text = "";
        boolean question = false;
        for (Item i : mSentence) {
            if (i.Speech != null && i.Speech.length() > 0) {

                if (i.hasFlag(Flag.toplace)
                        && text.toLowerCase().endsWith("go")) {
                    text += " to";
                }

                if (!text.endsWith(" "))
                    text += " ";
                text += i.Speech;
            }
            if (i.hasFlag(Flag.question))
                question = true;
        }
        if (question)
            text += "?";
        EditText tv = (EditText) findViewById(R.id.text);
        tv.setText(text + " ", TextView.BufferType.EDITABLE);
        tv.setSelection(text.length() + 1);
        tv.requestFocus();
        tv.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    }

    // Find a group item to go after the terminating speech item that's
    // just been added to the current sentence. Returns null if there isn't
    // one.
    private GroupItem findAfterGroup() {
        ListIterator<Item> li = mSentence.listIterator(mSentence.size() - 1);
        int used = 0;
        while (li.hasPrevious()) {
            Item i = li.previous();
            if (i.getClass() == SpeechItem.class
                    && ((SpeechItem) i).Goto == null)
                used += 1;
            if (i.After != null) {
                if (used == 0)
                    return (GroupItem) mLayout.RootItem.findItemByID(i.After);
                used -= 1;
            }
        }
        return null;
    }

    private class ItemClickListener implements View.OnClickListener {
        private GroupItem mGroup;
        private Integer mPosition;

        public ItemClickListener(GroupItem group, Integer position) {
            mGroup = group;
            mPosition = position;
        }

        @Override
        public void onClick(View view) {

            Item clickItem = mGroup.allItems().get(mPosition);
            if (clickItem.getClass() == GroupItem.class) {

                GroupItem gi = (GroupItem) clickItem;
                useSpeech(gi);
                mImageAdapter.setGroup(gi);

            } else if (clickItem.getClass() == SpeechItem.class) {

                SpeechItem si = (SpeechItem) clickItem;
                useSpeech(si);
                if (si.Goto == null) {
                    GroupItem tg = findAfterGroup();
                    if (tg != null) {
                        mImageAdapter.setGroup(tg);
                    } else {
                        // We've reached the end of any defined chain. We used
                        // to auto-speak here, but now I think it's better to
                        // just go back to the root level, leaving speaking to
                        // be done manually.
                        mImageAdapter.setGroup(mLayout.RootItem);
                    }
                } else {
                    GroupItem tg = (GroupItem) mLayout.RootItem
                            .findItemByID(si.Goto);
                    if (tg != null)
                        mImageAdapter.setGroup(tg);
                }

            }

        }

    }

    private class NewItemClickListener implements View.OnClickListener {
        private GroupItem mGroup;

        public NewItemClickListener(GroupItem group) {
            mGroup = group;
        }

        @Override
        public void onClick(View view) {
            newItem(mGroup, mGroup.Items.size(), false);
        }

    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private GroupItem mCurrentGroup;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public void setGroup(GroupItem group) {
            mCurrentGroup = group;
            notifyDataSetChanged();
        }

        public GroupItem getGroup() {
            return mCurrentGroup;
        }

        public int getCount() {
            // One extra item in edit mode, for the 'fake item' at the end.
            return mCurrentGroup.allItems().size() + (mEditable ? 1 : 0);
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // Create a new item...
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout lin;
            ImageView imageView;
            TextView textView;
            if (convertView == null) { // if it's not recycled, initialize some
                                       // attributes
                lin = new LinearLayout(mContext);
                lin.setOrientation(LinearLayout.VERTICAL);
                lin.setLayoutParams(new GridView.LayoutParams(105, 105));

                imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, 85));

                textView = new TextView(mContext);
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
                textView.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                lin.addView(imageView);
                lin.addView(textView);
            } else {
                lin = (LinearLayout) convertView;
                imageView = (ImageView) lin.getChildAt(0);
                textView = (TextView) lin.getChildAt(1);
            }

            if (mEditable && position == mCurrentGroup.allItems().size()) {
                imageView.setOnClickListener(new NewItemClickListener(
                        mCurrentGroup));
                imageView.setLongClickable(true);
                imageView.setImageResource(R.drawable.newitem);
                imageView.setBackgroundDrawable(null);
                imageView.setPadding(8, 8, 8, 8);
                textView.setText("new...");
            } else {
                imageView.setOnClickListener(new ItemClickListener(
                        mCurrentGroup, position));
                imageView.setLongClickable(true);

                Item item = mCurrentGroup.allItems().get(position);
                try {
                    imageView.setImageBitmap(item.Icon.getBitmap(Leo.this));
                } catch (Exception e) {
                }
                if (item.getClass() == GroupItem.class) {
                    imageView.setBackgroundResource(R.drawable.folder);
                    imageView.setPadding(18, 18, 18, 18);
                } else {
                    imageView.setBackgroundDrawable(null);
                    imageView.setPadding(8, 8, 8, 8);
                }
                textView.setText(item.Caption);
            }
            return lin;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        super.onCreateOptionsMenu(menu);
        menu.add(Menu.NONE, LAYOUTS, 1, R.string.menu_layouts).setIcon(
                android.R.drawable.ic_menu_slideshow);
        menu.add(Menu.NONE, HISTORY, 2, R.string.menu_history).setIcon(
                android.R.drawable.ic_menu_recent_history);
        menu.add(Menu.NONE, PREFERENCES, 3, R.string.menu_preferences).setIcon(
                android.R.drawable.ic_menu_preferences);
        menu.add(Menu.NONE, ABOUT, 3, R.string.menu_about).setIcon(
                android.R.drawable.ic_menu_help);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        case HISTORY:
            Intent hi = new Intent(getBaseContext(), History.class);
            try {
                hi.putExtra("history", mHistory.toXml());
            } catch (Exception e) {
                Log.d("Leo",
                        "Failed to read generate history xml - "
                                + e.getMessage());
                Log.v("Leo", Log.getStackTraceString(e));
                return true;
            }
            startActivityForResult(hi, REQUEST_HISTORY);
            return true;

        case LAYOUTS:

            // We need to at least be able to read external storage to do
            // anything with this, so check that first...
            String state = Environment.getExternalStorageState();
            if (!Environment.MEDIA_MOUNTED.equals(state)) {
                Toast.makeText(this, R.string.no_external, Toast.LENGTH_SHORT)
                        .show();
                return true;
            }
            Intent i = new Intent(getBaseContext(), Layouts.class);
            try {
                i.putExtra("layout", mLayout.toXml());
            } catch (Exception e) {
                Log.d("Leo",
                        "Failed to generate layout xml - " + e.getMessage());
                Log.v("Leo", Log.getStackTraceString(e));
                return true;
            }
            startActivityForResult(i, REQUEST_LAYOUTS);
            return true;

        case PREFERENCES:
            Intent prefs = new Intent(getBaseContext(), Preferences.class);
            startActivityForResult(prefs, REQUEST_PREFS);
            return true;

        case ABOUT:
            LayoutInflater li = LayoutInflater.from(this);
            View view = li.inflate(R.layout.about, null);

            // Fill in the version...
            TextView tv = (TextView) view.findViewById(R.id.version);
            PackageManager pm = getPackageManager();
            try {
                PackageInfo pi = pm.getPackageInfo(getApplicationContext()
                        .getPackageName(), 0);
                tv.setText(pi.versionName);
            } catch (Exception e) {
            }

            Builder p = new AlertDialog.Builder(this).setView(view);
            final AlertDialog alrt = p.create();
            alrt.setIcon(R.drawable.ic_launcher);
            alrt.setTitle(getString(R.string.about_title));
            alrt.setButton(AlertDialog.BUTTON_NEUTRAL,
                    getString(R.string.about_website),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                int whichButton) {
                            Uri uri = Uri
                                    .parse("http://projects.ciarang.com/p/leo");
                            startActivity(new Intent(Intent.ACTION_VIEW, uri));
                        }
                    });
            alrt.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                int whichButton) {
                        }
                    });
            alrt.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
        case REQUEST_PREFS:
            readPrefs();
            break;
        case REQUEST_HISTORY:
            if (resultCode == RESULT_OK) {
                if (data.hasExtra("selected")) {
                    TextView tv = (TextView) findViewById(R.id.text);
                    tv.setText(data.getStringExtra("selected"));
                }
            }
            break;
        case REQUEST_MOVETO:
            if (resultCode == RESULT_OK) {
                String toid = data.getStringExtra("itemid");
                GroupItem togroup = (GroupItem) mLayout.RootItem
                        .findItemByID(toid);
                Item moveitem = mLayout.RootItem.findItemByID(data
                        .getStringExtra("extra"));
                moveitem.Parent.Items.remove(moveitem);
                togroup.Items.add(moveitem);
                moveitem.Parent = togroup;
                mImageAdapter.notifyDataSetChanged();
            }
            break;
        case REQUEST_LAYOUTS:
        case REQUEST_ITEMEDIT:
            if (resultCode == RESULT_OK) {
                try {
                    mLayout = BoardLayout
                            .fromXml(data.getStringExtra("layout"));
                    // Need to refresh, because things have changed.
                    if (data.hasExtra("returnpath")) {
                        Item item = mLayout.RootItem;
                        String p = data.getStringExtra("returnpath");
                        Log.d("Leo", "Return path is " + p);
                        if (p.length() != 0) {
                            int idx;
                            while ((idx = p.indexOf('/')) != -1) {
                                item = ((GroupItem) item).allItems().get(
                                        Integer.parseInt(p.substring(0, idx)));
                                p = p.substring(idx + 1);
                            }
                            item = ((GroupItem) item).allItems().get(
                                    Integer.parseInt(p));
                        }
                        mImageAdapter.setGroup((GroupItem) item);
                    } else {
                        mImageAdapter.setGroup(mLayout.RootItem);
                    }

                } catch (Exception e) {
                    Log.d("Leo",
                            "Failed to read edited layout - " + e.getMessage());
                    for (StackTraceElement t : e.getStackTrace())
                        Log.d("Leo", t.toString());
                }
            }
            break;

        }
    }

    private BoardLayout getDefaultLayout() {

        BoardLayout layout = new BoardLayout();

        try {

            GroupItem grp, g2, g3;

            grp = new GroupItem(new LeoIcon(LeoIcon.Internals.I), "I", "I");

            g2 = new GroupItem(new LeoIcon(LeoIcon.Internals.Want), "want",
                    "want");

            g3 = new GroupItem(new LeoIcon(LeoIcon.Internals.Go), "to go",
                    "to go");
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Home), "home"));
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Park),
                    "to the park"));
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Outside),
                    "outside"));
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Inside),
                    "inside"));
            g2.add(g3);

            g3 = new GroupItem(new LeoIcon(LeoIcon.Internals.Eat), "to eat",
                    "to eat");
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Biscuit),
                    "a biscuit"));
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Biscuits),
                    "some biscuits"));
            g3.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Apple),
                    "an apple"));
            g2.add(g3);

            grp.add(g2);

            g2 = new GroupItem(new LeoIcon(LeoIcon.Internals.Like), "like",
                    "like");
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.That), "that"));
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Biscuits),
                    "biscuits"));
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Apple),
                    "apples"));
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Park),
                    "the park"));
            grp.add(g2);

            g2 = new GroupItem(new LeoIcon(LeoIcon.Internals.Dislike),
                    "don't like", "don't like");
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.That), "that"));
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Biscuits),
                    "biscuits"));
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Apple),
                    "apples"));
            g2.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Park),
                    "the park"));
            grp.add(g2);

            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Hungry),
                    "am hungry"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Hungry),
                    "am thirsty"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Hungry),
                    "am tired"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Hungry),
                    "am cold"));

            layout.RootItem.add(grp);

            grp = new GroupItem(new LeoIcon(LeoIcon.Internals.Animals),
                    "animals");
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Dog), "dog"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Cat), "cat"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Duck), "duck"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Chicken),
                    "chicken"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Fish), "fish"));
            grp.add(new SpeechItem(new LeoIcon(LeoIcon.Internals.Elephant),
                    "elephant"));
            layout.RootItem.add(grp);

            layout.RootItem.add(new SpeechItem(new LeoIcon(
                    LeoIcon.Internals.Yes), "yes"));
            layout.RootItem.add(new SpeechItem(
                    new LeoIcon(LeoIcon.Internals.No), "no"));
            layout.RootItem.add(new SpeechItem(new LeoIcon(
                    LeoIcon.Internals.Good), "good"));
            layout.RootItem.add(new SpeechItem(new LeoIcon(
                    LeoIcon.Internals.Bad), "bad"));
        } catch (Exception e) {
            Log.d("Leo",
                    "Exception while generating default layout - "
                            + e.getMessage());
        }

        return layout;
    }

    // Read frequently accessed preferences into local members.
    private void readPrefs() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        boolean olded = mEditable;
        mEditable = prefs.getBoolean("allowEdit", true);
        if (olded != mEditable)
            mImageAdapter.notifyDataSetChanged();

        boolean oldfs = mFullScreen;
        mFullScreen = prefs.getBoolean("fullscreen", false);
        if (oldfs != mFullScreen) {
            if (mFullScreen) {
                getWindow().setFlags(
                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                getWindow().setFlags(0,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
        }
    }

}

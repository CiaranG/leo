/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.ciarang.leo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

// Present a list of all the items in the supplied layout, and allow one to
// be chosen. The ID of the chosen item is returned to the calling activity.
// Long-clicking instead of clicking will return the ID prefixed by +.
public class ItemChooser extends ListActivity {

    private List<Item> mItems = new ArrayList<Item>();
    private BoardLayout mLayout;

    // An extra bit of data passed in with the intent, that just gets sent
    // back to the caller in the say way!
    private String mExtra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> onlyshow = null;
        List<String> donotshow = null;
        Intent intent = getIntent();
        mExtra = intent.getStringExtra("extra");
        try {
            mLayout = BoardLayout.fromXml(intent.getStringExtra("layout"));
        } catch (Exception e) {
            Log.d("Leo", "Failed to read layout - " + e.getMessage() + " - "
                    + e.getStackTrace()[0].toString());
            Log.d("Leo",
                    "Received layout definition of "
                            + intent.getStringExtra("layout"));
            finish();
            return;
        }

        String tos = intent.getStringExtra("onlyshow");
        if (tos != null)
            onlyshow = new ArrayList<String>(Arrays.asList(tos.split(",")));
        tos = intent.getStringExtra("donotshow");
        if (tos != null)
            donotshow = new ArrayList<String>(Arrays.asList(tos.split(",")));
        tos = intent.getStringExtra("title");
        if (tos != null)
            setTitle(tos);

        // Search for all items and add them to our list.
        // TODO: Currently we add just groups (and not the root one) but
        // ultimately we'll probably want to configure what to choose from
        // via the intent.
        ArrayList<GroupItem> searchgroups = new ArrayList<GroupItem>();
        searchgroups.add(mLayout.RootItem);
        while (!searchgroups.isEmpty()) {
            for (Item i : searchgroups.get(0).Items) {
                if (i.getClass() == GroupItem.class) {
                    String id = i.ID;
                    if ((onlyshow == null || onlyshow.contains(id))
                            && (donotshow == null || !donotshow.contains(id)))
                        mItems.add(i);
                    searchgroups.add((GroupItem) i);
                }
            }
            searchgroups.remove(0);
        }

        ListView lv = getListView();
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int pos,
                    long id) {
                Intent data = new Intent();
                try {
                    data.putExtra("itemid", "+" + mItems.get(pos).ID);
                    if (mExtra != null)
                        data.putExtra("extra", mExtra);
                } catch (Exception e) {
                }
                setResult(RESULT_OK, data);
                finish();
                return true;
            }
        });

        setListAdapter(new ArrayAdapter<Item>(this,
                android.R.layout.simple_list_item_1, mItems));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent data = new Intent();
        try {
            data.putExtra("itemid", mItems.get(position).ID);
            if (mExtra != null)
                data.putExtra("extra", mExtra);
        } catch (Exception e) {
        }
        setResult(RESULT_OK, data);
        finish();
    }

}

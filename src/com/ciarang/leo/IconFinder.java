/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.ciarang.leo;

import java.io.File;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class IconFinder extends Activity {

    private static final int REQUEST_CHOOSEICON = 0;
    private static final int REQUEST_CHOOSEPICTURE = 1;
    private static final int REQUEST_TAKEPICTURE = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.iconfinder);
        setTitle(R.string.iconfinder_title);

        TextView searchtext1 = (TextView) findViewById(R.id.searchtext1);
        TextView searchtext2 = (TextView) findViewById(R.id.searchtext2);

        Intent i = getIntent();
        if (i.hasExtra("searchhint")) {
            searchtext1.setText(i.getStringExtra("searchhint"));
            searchtext2.setText(i.getStringExtra("searchhint"));
        }

        // Show or hide the appropriate rows depending on whether we have
        // the ARASAAC icon library installed or not...
        View aradl1 = findViewById(R.id.dlarasaactxt);
        View ara1 = findViewById(R.id.arasaactxt);
        View aradl2 = findViewById(R.id.dlarasaacrow);
        View ara2 = findViewById(R.id.arasaacrow);
        File lib = new File(getExternalFilesDir(null), "arasaac_col_small.jar");
        if (lib.exists()) {
            aradl1.setVisibility(View.GONE);
            aradl2.setVisibility(View.GONE);
        } else {
            ara1.setVisibility(View.GONE);
            ara2.setVisibility(View.GONE);
        }

    }

    public void browseinternal(View v) {
        Intent i = new Intent(getBaseContext(), IconChooser.class);
        i.putExtra("type", "internal");
        startActivityForResult(i, REQUEST_CHOOSEICON);
    }

    public void searchlibrary(View v) {
        Intent i = new Intent(getBaseContext(), IconChooser.class);
        TextView st = (TextView) findViewById(R.id.searchtext1);
        i.putExtra("searchtext", st.getText().toString());
        i.putExtra("type", "library");
        startActivityForResult(i, REQUEST_CHOOSEICON);
    }

    public void downloadlibrary(View v) {
        String url = "http://ciarang.com/stuff/arasaac_col_small.jar";
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(url));
        request.setDescription("ARASAAC icon library");
        request.setTitle("Leo downloading");
        request.setDestinationInExternalFilesDir(this, null,
                "arasaac_col_small.jar");
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

        // Disable the button to avoid it being pressed again.
        Button b = (Button) findViewById(R.id.btnDownloadLibrary);
        b.setEnabled(false);

        Builder p = new AlertDialog.Builder(this);
        final AlertDialog alrt = p.create();
        alrt.setIcon(R.drawable.ic_launcher);
        alrt.setTitle("Downloading");
        alrt.setMessage("The ARASAAC icon library should now be downloading. You should see the progress in your status bar. When it's complete, return here and you will have access to the library.");
        alrt.show();
    }

    public void searchonline(View v) {
        Intent i = new Intent(getBaseContext(), IconChooser.class);
        TextView st = (TextView) findViewById(R.id.searchtext2);
        i.putExtra("searchtext", st.getText().toString());
        i.putExtra("type", "online");
        startActivityForResult(i, REQUEST_CHOOSEICON);
    }

    public void takepicture(View v) {
        // TODO: We need to check if there is a camera available first,
        // because this is going to crash if there isn't!
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.d("Leo", "Sending ACTION_IMAGE_CAPTURE");
        startActivityForResult(i, REQUEST_TAKEPICTURE);
    }

    public void choosepicture(View v) {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        Log.d("Leo", "Sending ACTION_GET_CONTENT");
        startActivityForResult(i, REQUEST_CHOOSEPICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
        case REQUEST_CHOOSEICON:
            if (resultCode == RESULT_OK) {
                // We just pass this straight back up the line to the
                // original caller...
                Intent data2 = new Intent();
                data2.putExtra("icon", data.getStringExtra("icon"));
                setResult(RESULT_OK, data2);
                finish();
            }
            break;
        case REQUEST_CHOOSEPICTURE:
            if (resultCode == RESULT_OK) {
                InputStream stream;
                Bitmap b1;
                try {
                    stream = getContentResolver().openInputStream(
                            data.getData());
                    b1 = BitmapFactory.decodeStream(stream);
                    stream.close();
                } catch (Exception e) {
                    finish();
                    return;
                }
                Bitmap b2 = Bitmap.createScaledBitmap(b1, 85, 85, false);
                b1.recycle();
                LeoIcon icon = new LeoIcon(b2);
                Intent data2 = new Intent();
                data2.putExtra("icon", icon.Encoded());
                setResult(RESULT_OK, data2);
                finish();
            }
            break;
        case REQUEST_TAKEPICTURE:
            if (resultCode == RESULT_OK) {
                Bitmap b = (Bitmap) data.getExtras().get("data");
                LeoIcon icon = new LeoIcon(b);
                Intent data2 = new Intent();
                data2.putExtra("icon", icon.Encoded());
                setResult(RESULT_OK, data2);
                finish();
            } else {
                Log.d("Leo", "Unexpected result code from REQUEST_TAKEPICTURE");
            }
        }
    }

}

/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.ciarang.leo;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Layouts extends Activity {

    ListView mListView;
    LayoutAdapter mLayoutAdapter;
    BoardLayout mCurLayout;

    boolean mEditable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        readPrefs();

        Intent i = getIntent();
        try {
            mCurLayout = BoardLayout.fromXml(i.getStringExtra("layout"));
        } catch (Exception e) {
            Log.d("Leo", "Failed to read layout - " + e.getMessage() + " - "
                    + e.getStackTrace()[0].toString());
            Log.d("Leo",
                    "Received layout definition of "
                            + i.getStringExtra("layout"));
            finish();
            return;
        }

        setContentView(R.layout.layouts);

        mListView = (ListView) findViewById(R.id.listview);
        mLayoutAdapter = new LayoutAdapter(this);
        mListView.setAdapter(mLayoutAdapter);

        refreshList();

    }

    void refreshList() {

        mLayoutAdapter.Files.clear();
        File dir = getLayoutsDirectory();
        if (dir != null) {
            for (File f : dir.listFiles()) {
                try {
                    String name = f.getName();
                    if (name.endsWith(".xml")) {
                        mLayoutAdapter.Files.add(name.substring(0,
                                name.length() - 4));
                    }
                } catch (Exception e) {
                }
            }
        }
        mLayoutAdapter.notifyDataSetChanged();

    }

    // Finish the activity and return RESULT_OK and mCurLayout to the
    // caller.
    void finishOk() {
        Intent data = new Intent();
        try {
            data.putExtra("layout", mCurLayout.toXml());
        } catch (Exception e) {
        }
        setResult(RESULT_OK, data);
        finish();
    }

    private class LayoutAdapter extends BaseAdapter {
        private Context mContext;
        public ArrayList<String> Files = new ArrayList<String>();

        public LayoutAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return Files.size() + 1;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // Create a new item...
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.layoutlistitem, null);
            }

            TextView tv = (TextView) v.findViewById(R.id.title);
            TextView tvi = (TextView) v.findViewById(R.id.info);
            Button share = (Button) v.findViewById(R.id.share);
            Button load = (Button) v.findViewById(R.id.load);
            Button save = (Button) v.findViewById(R.id.save);
            Button saveas = (Button) v.findViewById(R.id.saveas);
            Button delete = (Button) v.findViewById(R.id.delete);
            if (position == 0) {
                tv.setText(R.string.current);
                load.setVisibility(View.GONE);
                saveas.setVisibility(mEditable ? View.VISIBLE : View.GONE);
                delete.setVisibility(View.GONE);
                if (mCurLayout.Modified) {
                    save.setVisibility(mCurLayout.Name == null || !mEditable ? View.GONE
                            : View.VISIBLE);
                } else {
                    save.setVisibility(View.GONE);
                }
                if (mCurLayout.Name != null) {
                    if (mCurLayout.Modified)
                        tvi.setText(String.format(
                                mContext.getString(R.string.basedon),
                                mCurLayout.Name));
                    else
                        tvi.setText(mCurLayout.Name);
                } else {
                    if (mCurLayout.Modified)
                        tvi.setText(R.string.modified);
                    else
                        tvi.setText(R.string.notmodified);
                }
            } else {
                tv.setText(Files.get(position - 1));
                load.setVisibility(View.VISIBLE);
                saveas.setVisibility(View.GONE);
                save.setVisibility(View.GONE);
                delete.setVisibility(mEditable ? View.VISIBLE : View.GONE);
            }
            delete.setTag(position);
            load.setTag(position);
            share.setTag(position);

            share.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int i = (Integer) v.getTag();
                    shareLayout(i);
                }
            });

            delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int i = (Integer) v.getTag();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            mContext);
                    builder.setMessage(R.string.suredelete)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            String name = Files.get(i - 1);
                                            File dir = getLayoutsDirectory();
                                            File f = new File(dir, name
                                                    + ".xml");
                                            f.delete();
                                            if (mCurLayout.Name.equals(name))
                                                mCurLayout.Modified = true;
                                            refreshList();
                                        }
                                    })
                            .setNegativeButton(R.string.cancel,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            saveas.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            Layouts.this);
                    alert.setTitle(R.string.saveas);
                    alert.setMessage(R.string.enter_name);
                    final EditText input = new EditText(Layouts.this);
                    alert.setView(input);
                    alert.setPositiveButton(R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    String name = input.getText().toString();
                                    if (Files.contains(name)) {
                                        Toast.makeText(mContext,
                                                R.string.already_exists,
                                                Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                    mCurLayout.Name = name;
                                    mCurLayout.Modified = false;
                                    File dir = getLayoutsDirectory();
                                    if (dir == null) {
                                        Toast.makeText(
                                                Layouts.this,
                                                "Can't create layout directory",
                                                Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                    File f = new File(dir, mCurLayout.Name
                                            + ".xml");
                                    try {
                                        FileUtils.writeStringToFile(f,
                                                mCurLayout.toXml());
                                    } catch (Exception ex) {
                                        Toast.makeText(Layouts.this,
                                                ex.getMessage(),
                                                Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                    Toast.makeText(Layouts.this,
                                            R.string.saved, Toast.LENGTH_SHORT)
                                            .show();
                                    finishOk();
                                    return;
                                }
                            });

                    alert.setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    dialog.cancel();
                                }
                            });
                    alert.show();
                }

            });
            save.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        File dir = getLayoutsDirectory();
                        if (dir == null) {
                            Toast.makeText(Layouts.this,
                                    "Can't create layout directory",
                                    Toast.LENGTH_LONG).show();
                            return;
                        }
                        File f = new File(dir, mCurLayout.Name + ".xml");
                        mCurLayout.Modified = false;
                        FileUtils.writeStringToFile(f, mCurLayout.toXml());
                        Toast.makeText(Layouts.this, R.string.saved,
                                Toast.LENGTH_SHORT).show();
                        finishOk();
                    } catch (Exception ex) {
                        Toast.makeText(Layouts.this, ex.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
            load.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int i = (Integer) v.getTag();

                    // Simple case - the current layout isn't modified, so
                    // just do it...
                    if (!mCurLayout.Modified) {
                        loadLayout(i);
                        return;
                    }

                    // The user has a modified layout, so check with them
                    // before overwriting it...
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            mContext);
                    builder.setMessage(R.string.sureload)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            loadLayout(i);
                                        }
                                    }).setNegativeButton(R.string.cancel, null);
                    AlertDialog alert = builder.create();
                    alert.show();

                }

            });

            return v;
        }

        // Load the given layout and exit.
        void loadLayout(int i) {
            try {

                String name = Files.get(i - 1);
                File dir = getLayoutsDirectory();
                File f = new File(dir, name + ".xml");
                String xml = FileUtils.readFileToString(f, "utf-8");
                mCurLayout = BoardLayout.fromXml(xml);
                mCurLayout.PreloadIcons(getBaseContext());
                mCurLayout.Name = name;
                finishOk();
            } catch (Exception ex) {
                Toast.makeText(Layouts.this, ex.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }
        }

        // Share the given layout
        void shareLayout(int i) {
            try {
                String xml;
                if(i==0) {
                    xml = mCurLayout.toXml();
                } else {
                    String name = Files.get(i - 1);
                    File dir = getLayoutsDirectory();
                    File f = new File(dir, name + ".xml");
                    xml = FileUtils.readFileToString(f, "utf-8");                    
                }
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/xml");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, xml);
                startActivity(Intent.createChooser(sharingIntent, null));
            } catch (Exception ex) {
                Toast.makeText(Layouts.this, ex.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    // Get the external storage directory we use for storing layouts,
    // creating it if it doesn't already exist.
    File getLayoutsDirectory() {
        File dir = Environment.getExternalStorageDirectory();
        File dir2 = new File(dir, "Leo");
        if (dir2.exists())
            return dir2;
        if (!dir2.mkdir())
            return null;
        return dir2;
    }

    // Read frequently accessed preferences into local members.
    private void readPrefs() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        mEditable = prefs.getBoolean("allowEdit", true);
        if (prefs.getBoolean("fullscreen", false))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}

package com.ciarang.leo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class GroupItem extends Item {

    // The items in the group. These are the items that are actually in it.
    // Additional items can be included - they can't be accessed here, but
    // will be included in the list returned by allItems().
    public List<Item> Items;

    // List of IDs of items to include. Group items can be prefixed by a +
    // to include the contents of the group instead of the group itself.
    public List<String> Include;

    public GroupItem(LeoIcon icon, String caption) {
        this(icon, caption, null);
    }

    public GroupItem(LeoIcon icon, String caption, String speech) {
        Caption = caption;
        Speech = speech;
        Icon = icon;
        Items = new ArrayList<Item>();
        Include = new ArrayList<String>();
        Parent = null;
    }

    public void add(Item item) {
        item.Parent = this;
        Items.add(item);
    }

    public void insert(Item item, int before) {
        item.Parent = this;
        Items.add(before, item);
    }

    // Find an item, in this group and all its children, that has the given
    // ID. Returns null if not found.
    // We could have an index, instead of this search everything thing, but
    // I don't think it will ever be big enough for it to matter.
    public Item findItemByID(String id) {
        ArrayList<GroupItem> searchgroups = new ArrayList<GroupItem>();
        searchgroups.add(this);
        while (!searchgroups.isEmpty()) {
            for (Item i : searchgroups.get(0).Items) {
                if (i.ID.equals(id))
                    return i;
                if (i.getClass() == GroupItem.class)
                    searchgroups.add((GroupItem) i);
            }
            searchgroups.remove(0);
        }
        return null;
    }

    // Get all the items in the group, INCLUDING those that are included.
    // The included items ALWAYS come after the real items.
    public List<Item> allItems() {

        if (Include.size() == 0)
            return Items;

        List<Item> items = new ArrayList<Item>(Items);

        // Go up to the root item so we can find the included group...
        Item rootitem = this;
        while (rootitem.Parent != null)
            rootitem = rootitem.Parent;

        for (String id : Include) {
            boolean contents = false;
            if (id.startsWith("+")) {
                contents = true;
                id = id.substring(1);
            }
            Item item = ((GroupItem) rootitem).findItemByID(id);
            if (item != null) {
                if (!contents) {
                    items.add(item);
                } else {
                    // TODO: potential to infinitely loop here, if someone is
                    // daft
                    // enough to set up a circular inclusion!
                    for (Item incitem : ((GroupItem) item).allItems()) {
                        items.add(incitem);
                    }
                }
            }
        }

        return items;
    }

    public void ToXML(XmlSerializer s) throws IllegalArgumentException,
            IllegalStateException, IOException {
        s.startTag("", "groupitem");
        CommonToXML(s);
        if (Speech != null && Speech.length() > 0) {
            s.startTag("", "speech");
            s.text(Speech);
            s.endTag("", "speech");
        }
        if (Include.size() != 0) {
            s.startTag("", "include");
            boolean first = true;
            for (String id : Include) {
                if (first)
                    first = false;
                else
                    s.text(",");
                s.text(id);
            }
            s.endTag("", "include");
        }
        for (Item item : Items)
            item.ToXML(s);
        s.endTag("", "groupitem");
    }

    public static GroupItem FromXml(XmlPullParser parser) throws Exception {
        GroupItem g = new GroupItem(new LeoIcon(LeoIcon.Internals.Unknown), "");
        int eventType = parser.next();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
            case XmlPullParser.START_TAG:
                String name = parser.getName();
                if (!handleCommonTag(name, g, parser)) {

                    if (name.equals("include")) {
                        parser.next();
                        for (String id : parser.getText().split(","))
                            g.Include.add(id);
                    } else if (name.equals("groupitem")) {
                        g.Items.add(GroupItem.FromXml(parser));
                    } else if (name.equals("speechitem")) {
                        g.Items.add(SpeechItem.FromXml(parser));
                    } else if (name.equals("backitem")) {
                        // We don't have backitems any more, but need to skip
                        // over them for reading old XML.
                        // TODO: This can be removed shortly.
                        eventType = parser.next();
                        while (eventType != XmlPullParser.END_DOCUMENT) {
                            if (eventType == XmlPullParser.END_TAG
                                    && parser.getName().equals("backitem"))
                                break;
                            eventType = parser.next();
                        }
                    }
                }
                break;
            case XmlPullParser.END_TAG:
                if (parser.getName().equals("groupitem")) {
                    for (Item i : g.Items)
                        i.Parent = g;
                    return g;
                }
                break;
            }
            eventType = parser.next();
        }
        throw new Exception("Unexpected end of document");

    }
}

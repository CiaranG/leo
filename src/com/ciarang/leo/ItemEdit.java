/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.ciarang.leo;

import java.util.ArrayList;
import java.util.List;

import com.ciarang.leo.Item.Flag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemEdit extends Activity {

    private static final int REQUEST_CHOOSEICON = 0;
    private static final int REQUEST_INCLUDE = 1;
    private static final int REQUEST_REMOVEINCLUDE = 2;
    private static final int REQUEST_GOTO = 3;
    private static final int REQUEST_AFTER = 4;
    private static final int REQUEST_ADDFLAG = 5;
    private static final int REQUEST_REMOVEFLAG = 6;

    // The layout we're editing
    private BoardLayout mLayout;

    // The path to the item to edit. See Item.getPath for the format
    // of this.
    private String mPath;

    // The actual item we're editing.
    private Item mItem;

    // True when we're mirroring text entry in the caption to the speech
    // field.
    private boolean mSpeechMirror;

    private List<String> mIncludes = new ArrayList<String>();
    private String mGoto;
    private String mAfter;

    // The path the caller wants to return to.
    private String mReturnPath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.itemedit);
        setTitle(R.string.edititem_title);

        Intent i = getIntent();
        try {
            mLayout = BoardLayout.fromXml(i.getStringExtra("layout"));
        } catch (Exception e) {
            Log.d("Leo", "Failed to read edit layout - " + e.getMessage()
                    + " - " + e.getStackTrace()[0].toString());
            Log.d("Leo",
                    "Received layout definition of "
                            + i.getStringExtra("layout"));
            finish();
            return;
        }
        mPath = i.getStringExtra("path");
        Log.d("Leo", "Item edit path is " + mPath);
        if (i.hasExtra("returnpath"))
            mReturnPath = i.getStringExtra("returnpath");
        else
            mReturnPath = null;

        mSpeechMirror = false;

        // Find the actual item from the path, creating a new one if
        // necessary...
        Item item = mLayout.RootItem;
        String p = mPath;
        int idx;
        while ((idx = p.indexOf('/')) != -1) {
            item = ((GroupItem) item).Items.get(Integer.parseInt(p.substring(0,
                    idx)));
            p = p.substring(idx + 1);
        }
        if (p.startsWith("+g")) {
            try {
                mItem = new GroupItem(new LeoIcon(LeoIcon.Internals.Unknown),
                        "");
            } catch (Exception e) {
            }
            ((GroupItem) item).insert(mItem, Integer.parseInt(p.substring(2)));
        } else if (p.startsWith("+s")) {
            try {
                mItem = new SpeechItem(new LeoIcon(LeoIcon.Internals.Unknown),
                        "", "");
                mSpeechMirror = true;
            } catch (Exception e) {
            }
            ((GroupItem) item).insert(mItem, Integer.parseInt(p.substring(2)));
        } else {
            mItem = ((GroupItem) item).Items.get(Integer.parseInt(p));
        }

        // Find the views...
        final TextView tvcaption = (TextView) findViewById(R.id.editCaption);
        final TextView tvspeech = (TextView) findViewById(R.id.editSpeech);
        final ImageView pbicon = (ImageView) findViewById(R.id.editIcon);
        final View includerow = findViewById(R.id.includeRow);
        final View gotorow = findViewById(R.id.gotoRow);
        final Button gotoadd = (Button) findViewById(R.id.addgoto);
        final Button gotoremove = (Button) findViewById(R.id.removegoto);
        final Button afteradd = (Button) findViewById(R.id.addafter);
        final Button afterremove = (Button) findViewById(R.id.removeafter);
        final Button includeadd = (Button) findViewById(R.id.addinclude);
        final Button includeremove = (Button) findViewById(R.id.removeinclude);
        final Button flagadd = (Button) findViewById(R.id.addflag);
        final Button flagremove = (Button) findViewById(R.id.removeflag);
        
        updateFlagsUI();
        flagadd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), FlagChooser.class);
                try {
                    i.putExtra("layout", mLayout.toXml());
                    String lst = mItem.ID;
                    for (Flag f : mItem.Flags) {
                        if (lst.length() > 0)
                            lst += ",";
                        lst += f.toString();
                    }
                    i.putExtra("donotshow", lst);
                    i.putExtra("title", R.string.chooseflag);
                } catch (Exception e) {
                    return;
                }
                startActivityForResult(i, REQUEST_ADDFLAG);
            }
        });
        flagremove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItem.Flags.size() == 0)
                    return;
                if (mItem.Flags.size() == 1) {
                    mItem.Flags.remove(0);
                    updateFlagsUI();
                    return;
                }
                Intent i = new Intent(getBaseContext(), FlagChooser.class);
                try {
                    i.putExtra("layout", mLayout.toXml());
                    String lst = "";
                    for (Flag f : mItem.Flags) {
                        if (lst.length() > 0)
                            lst += ",";
                        lst += f.toString();
                    }
                    i.putExtra("onlyshow", lst);
                    i.putExtra("title", R.string.choosenotflag);
                } catch (Exception e) {
                    return;
                }
                startActivityForResult(i, REQUEST_REMOVEFLAG);
            }
        });
        
        
        mAfter = mItem.After;
        updateAfterUI();
        afteradd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), ItemChooser.class);
                try {
                    i.putExtra("layout", mLayout.toXml());
                    i.putExtra("title", R.string.chooseafter);
                } catch (Exception e) {
                    return;
                }
                startActivityForResult(i, REQUEST_AFTER);
            }
        });
        afterremove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mAfter = null;
                updateAfterUI();
            }
        });
        
        
        if (mItem.getClass() == SpeechItem.class) {
            gotorow.setVisibility(View.VISIBLE);
            mGoto = ((SpeechItem) mItem).Goto;
            updateGotoUI();
            gotoadd.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getBaseContext(), ItemChooser.class);
                    try {
                        i.putExtra("layout", mLayout.toXml());
                        i.putExtra("title", R.string.choosegoto);
                    } catch (Exception e) {
                        return;
                    }
                    startActivityForResult(i, REQUEST_GOTO);
                }
            });
            gotoremove.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mGoto = null;
                    updateGotoUI();
                }
            });
        } else {
            gotorow.setVisibility(View.GONE);
        }

        if (mItem.getClass() == GroupItem.class) {
            includerow.setVisibility(View.VISIBLE);
            for (String id : ((GroupItem) mItem).Include) {
                mIncludes.add(id);
            }
            updateIncludeUI();
            includeadd.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getBaseContext(), ItemChooser.class);
                    try {
                        i.putExtra("layout", mLayout.toXml());
                        String lst = mItem.ID;
                        for (String id : mIncludes) {
                            if (lst.length() > 0)
                                lst += ",";
                            lst += id;
                        }
                        i.putExtra("donotshow", lst);
                        i.putExtra("title", R.string.chooseinclude);
                    } catch (Exception e) {
                        return;
                    }
                    startActivityForResult(i, REQUEST_INCLUDE);
                }
            });
            includeremove.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIncludes.size() == 0)
                        return;
                    if (mIncludes.size() == 1) {
                        mIncludes.remove(0);
                        updateIncludeUI();
                        return;
                    }
                    Intent i = new Intent(getBaseContext(), ItemChooser.class);
                    try {
                        i.putExtra("layout", mLayout.toXml());
                        String lst = "";
                        for (String id : mIncludes) {
                            if (lst.length() > 0)
                                lst += ",";
                            if(id.startsWith("+"))
                                id = id.substring(1);
                            lst += id;
                        }
                        i.putExtra("onlyshow", lst);
                        i.putExtra("title", R.string.choosenotinclude);
                    } catch (Exception e) {
                        return;
                    }
                    startActivityForResult(i, REQUEST_REMOVEINCLUDE);
                }
            });
        } else {
            includerow.setVisibility(View.GONE);
        }

        tvcaption.setText(mItem.Caption);
        tvspeech.setText(mItem.Speech);
        try {
            pbicon.setImageBitmap(mItem.Icon.getBitmap(this));
        } catch (Exception e) {
        }
        if (mItem.getClass() == GroupItem.class) {
            pbicon.setBackgroundResource(R.drawable.folder);
            pbicon.setPadding(20, 20, 20, 20);
        }

        if (mSpeechMirror) {
            tvcaption.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable e) {
                    if (mSpeechMirror)
                        tvspeech.setText(e.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                        int arg2, int arg3) {
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1,
                        int arg2, int arg3) {
                }
            });
            tvspeech.setOnFocusChangeListener(new OnFocusChangeListener() {

                @Override
                public void onFocusChange(View tv, boolean hasFocus) {
                    if (hasFocus)
                        mSpeechMirror = false;
                }
            });
        }

        pbicon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), IconFinder.class);
                i.putExtra("searchhint", tvcaption.getText().toString());
                startActivityForResult(i, REQUEST_CHOOSEICON);
            }
        });

        Button okbutton = (Button) findViewById(R.id.okbutton);
        okbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mItem.Caption = tvcaption.getText().toString();
                String speechtxt = tvspeech.getText().toString().trim();
                if(speechtxt.length()==0)
                    mItem.Speech = null;
                else
                    mItem.Speech = speechtxt; 
                if (mItem.getClass() == GroupItem.class)
                    ((GroupItem) mItem).Include = mIncludes;
                if (mItem.getClass() == SpeechItem.class)
                    ((SpeechItem) mItem).Goto = mGoto;
                mItem.After = mAfter;

                Intent data = new Intent();
                mLayout.Modified = true;
                try {
                    data.putExtra("layout", mLayout.toXml());
                } catch (Exception e) {
                }
                if (mReturnPath != null)
                    data.putExtra("returnpath", mReturnPath);
                setResult(RESULT_OK, data);
                finish();
            }
        });
        Button cancelbutton = (Button) findViewById(R.id.cancelbutton);
        cancelbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        Button deletebutton = (Button) findViewById(R.id.deletebutton);
        if (mPath.endsWith("+g") || mPath.endsWith("+s")) {
            deletebutton.setVisibility(View.GONE);
        } else {
            deletebutton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItem.Parent.Items.remove(mItem);
                    Intent data = new Intent();
                    mLayout.Modified = true;
                    try {
                        data.putExtra("layout", mLayout.toXml());
                    } catch (Exception e) {
                    }
                    if (mReturnPath != null)
                        data.putExtra("returnpath", mReturnPath);
                    setResult(RESULT_OK, data);
                    finish();
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void updateFlagsUI() {
        String txt = "";
        for (Flag f : mItem.Flags) {
            if (txt.length() != 0)
                txt += ",";
            txt += f.toString();
        }
        if (txt.length() == 0)
            txt = "<none>";
        final TextView flagtext = (TextView) findViewById(R.id.textflags);
        flagtext.setText(txt);

        final Button flagremove = (Button) findViewById(R.id.removeflag);
        flagremove.setEnabled(mItem.Flags.size() > 0);
    }

    private void updateIncludeUI() {
        String txt = "";
        for (String id : mIncludes) {
            boolean contents = false;
            if (id.startsWith("+")) {
                contents = true;
                id = id.substring(1);
            }
            Item titem = mLayout.RootItem.findItemByID(id);
            if (titem != null) {
                if (txt.length() != 0)
                    txt += ",";
                if (contents)
                    txt += "+";
                txt += titem.toString();
            }
        }
        if (txt.length() == 0)
            txt = "<none>";
        final TextView includetext = (TextView) findViewById(R.id.textinclude);
        includetext.setText(txt);

        final Button includeremove = (Button) findViewById(R.id.removeinclude);
        includeremove.setEnabled(mIncludes.size() > 0);
    }

    private void updateAfterUI() {
        String txt;
        if (mAfter == null) {
            txt = "<none>";
        } else {
            Item titem = mLayout.RootItem.findItemByID(mAfter);
            txt = titem.toString();
        }
        final TextView aftertext = (TextView) findViewById(R.id.textafter);
        aftertext.setText(txt);

        final Button afterremove = (Button) findViewById(R.id.removeafter);
        afterremove.setEnabled(mAfter != null);
    }

    private void updateGotoUI() {
        String txt;
        if (mGoto == null) {
            txt = "<none>";
        } else {
            Item titem = mLayout.RootItem.findItemByID(mGoto);
            txt = titem.toString();
        }
        final TextView gototext = (TextView) findViewById(R.id.textgoto);
        gototext.setText(txt);

        final Button gotoremove = (Button) findViewById(R.id.removegoto);
        gotoremove.setEnabled(mGoto != null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
        case REQUEST_CHOOSEICON:
            if (resultCode == RESULT_OK) {
                try {
                    LeoIcon icon = LeoIcon.Decode(data.getStringExtra("icon"));
                    ImageView pbicon = (ImageView) findViewById(R.id.editIcon);
                    pbicon.setImageBitmap(icon.getBitmap(this));
                    mItem.Icon = icon;
                } catch (Exception e) {
                    Log.d("Leo",
                            "Failed to read chosen icon - " + e.getMessage()
                                    + " - " + e.getStackTrace()[0].toString());
                }
            }
            break;
        case REQUEST_INCLUDE:
            if (resultCode == RESULT_OK) {
                String newid = data.getStringExtra("itemid");
                if (!mIncludes.contains(newid)) {
                    mIncludes.add(newid);
                    updateIncludeUI();
                }
            }
            break;
        case REQUEST_REMOVEINCLUDE:
            if (resultCode == RESULT_OK) {
                String newid = data.getStringExtra("itemid");
                if (mIncludes.contains(newid)
                        || mIncludes.contains("+" + newid)) {
                    mIncludes.remove(newid);
                    mIncludes.remove("+" + newid);
                    updateIncludeUI();
                }
            }
            break;
        case REQUEST_ADDFLAG:
            if (resultCode == RESULT_OK) {
                Flag f = Flag.valueOf(data.getStringExtra("flag"));
                if (!mItem.Flags.contains(f)) {
                    mItem.Flags.add(f);
                    updateFlagsUI();
                }
            }
            break;
        case REQUEST_REMOVEFLAG:
            if (resultCode == RESULT_OK) {
                Flag f = Flag.valueOf(data.getStringExtra("flag"));
                if (mItem.Flags.contains(f)) {
                    mItem.Flags.remove(f);
                    updateFlagsUI();
                }
            }
            break;
        case REQUEST_GOTO:
            if (resultCode == RESULT_OK) {
                String newid = data.getStringExtra("itemid");
                if (newid.startsWith("+")) {
                    // TODO: We're ignoring the long-clicked version for a
                    // goto, so why are we letting them do it?
                    newid = newid.substring(1);
                }
                mGoto = newid;
                updateGotoUI();
            }
            break;
        case REQUEST_AFTER:
            if (resultCode == RESULT_OK) {
                String newid = data.getStringExtra("itemid");
                if (newid.startsWith("+")) {
                    // TODO: We're ignoring the long-clicked version for a
                    // goto, so why are we letting them do it?
                    newid = newid.substring(1);
                }
                mAfter = newid;
                updateAfterUI();
            }
            break;

        }
    }

}

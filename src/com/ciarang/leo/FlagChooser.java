/*
 * Copyright (C) 2012-2013  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.ciarang.leo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ciarang.leo.Item.Flag;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

// Present a list of all the items in the supplied layout, and allow one to
// be chosen. The ID of the chosen item is returned to the calling activity.
// Long-clicking instead of clicking will return the ID prefixed by +.
public class FlagChooser extends ListActivity {

    private List<Flag> mFlags = new ArrayList<Flag>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> onlyshow = null;
        List<String> donotshow = null;
        Intent intent = getIntent();

        String tos = intent.getStringExtra("onlyshow");
        if (tos != null)
            onlyshow = new ArrayList<String>(Arrays.asList(tos.split(",")));
        tos = intent.getStringExtra("donotshow");
        if (tos != null)
            donotshow = new ArrayList<String>(Arrays.asList(tos.split(",")));
        tos = intent.getStringExtra("title");
        if (tos != null)
            setTitle(tos);

        // Add the relevant flags to our list.
        // TODO: Currently we add just groups (and not the root one) but
        // ultimately we'll probably want to configure what to choose from
        // via the intent.
        for(Flag f : Flag.values()) {
            String sf = f.toString();
            if ((onlyshow == null || onlyshow.contains(sf))
                    && (donotshow == null || !donotshow.contains(sf)))
                mFlags.add(f);
        }

        setListAdapter(new ArrayAdapter<Flag>(this,
                android.R.layout.simple_list_item_1, mFlags));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent data = new Intent();
        try {
            data.putExtra("flag", mFlags.get(position).toString());
        } catch (Exception e) {
        }
        setResult(RESULT_OK, data);
        finish();
    }

}

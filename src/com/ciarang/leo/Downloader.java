/*
 * Copyright (C) 2010-2013 Ciaran Gultnieks <ciaran@ciarang.com>
 * Copyright (C) 2011 Henrik Tunedal <tunedal@gmail.com>
 *
 * Adapted from fdroidclient (https://f-droid.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package com.ciarang.leo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import android.util.Log;

public class Downloader extends Thread {

    private String url;
    private File destfile;

    public static enum Status {
        STARTING, RUNNING, ERROR, DONE, CANCELLED
    };

    public static enum Error {
        CORRUPT, UNKNOWN
    };

    private Status status = Status.STARTING;
    private Error error;
    private int progress;
    private int max;
    private String errorMessage;

    // Constructor - creates a Downloader to download the file pointed to by
    // the given URL.
    Downloader(String url, File destfile) {
        this.url = url;
        this.destfile = destfile;
    }

    public synchronized Status getStatus() {
        return status;
    }

    // Current progress and maximum value for progress dialog
    public synchronized int getProgress() {
        return progress;
    }

    public synchronized int getMax() {
        return max;
    }

    // Error code and error message, only valid if status is ERROR
    public synchronized Error getErrorType() {
        return error;
    }

    public synchronized String getErrorMessage() {
        return errorMessage;
    }

    // The URL being downloaded
    public synchronized String remoteFile() {
        return url;
    }

    @Override
    public void run() {

        Log.d("Leo", "Download of " + url + " starting");
        synchronized (this) {
            progress = 0;
            // TODO: Get the actual file size you clown!
            max = 75527296;
            status = Status.RUNNING;
        }
        InputStream input = null;
        OutputStream output = null;
        try {

            input = new URL(url).openStream();
            output = new FileOutputStream(destfile);
            byte data[] = new byte[Utils.BUFFER_SIZE];
            while (true) {
                if (isInterrupted()) {
                    Log.d("Leo", "Download cancelled!");
                    break;
                }
                int count = input.read(data);
                if (count == -1) {
                    break;
                }
                output.write(data, 0, count);
                synchronized (this) {
                    progress += count;
                }
            }

            if (isInterrupted()) {
                destfile.delete();
                synchronized (this) {
                    status = Status.CANCELLED;
                }
                return;
            }
        } catch (Exception e) {
            Log.e("Leo", "Download failed:\n" + Log.getStackTraceString(e));
            synchronized (this) {
                destfile.delete();
                error = Error.UNKNOWN;
                errorMessage = e.toString();
                status = Status.ERROR;
                return;
            }
        } finally {
            Utils.closeQuietly(output);
            Utils.closeQuietly(input);
        }

        Log.d("Leo", "Download finished: " + destfile);
        synchronized (this) {
            status = Status.DONE;
        }
    }
}

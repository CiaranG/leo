package com.ciarang.leo;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class History extends ListActivity {

    HistoryData mHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        readPrefs();
        
        setTitle(R.string.history_title);
        
        Intent i = getIntent();
        try {
            mHistory = HistoryData.fromXml(i.getStringExtra("history"));
        } catch (Exception e) {
            mHistory = new HistoryData();
        }

        final LayoutInflater li = LayoutInflater.from(this);

        setListAdapter(new ArrayAdapter<String>(this,
                R.layout.history_list_item, mHistory.Data) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row;
                if (convertView == null)
                    row = li.inflate(R.layout.history_list_item, null);
                else
                    row = convertView;
                TextView tv = (TextView) row.findViewById(android.R.id.text1);
                // Sneaky list order reversal here...
                tv.setText(getItem(this.getCount() - 1 - position));
                return row;
            }
        });

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String selected = mHistory.Data.get(mHistory.Data.size() - 1 - position);
        Intent i = new Intent();
        i.putExtra("selected", selected);
        setResult(RESULT_OK, i);
        finish();
    }

    // Read frequently accessed preferences into local members.
    private void readPrefs() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        if(prefs.getBoolean("fullscreen", false))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    
    
}

package com.ciarang.leo;

import com.ciarang.leo.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

public class LeoIcon {

    public enum Internals {
        External, Library, Unknown, Back, Dog, Cat, Duck, Chicken, Fish, Elephant, Yes, No, Good, Bad, Animals, Want, Go, Eat, Biscuit, Biscuits, Apple, Home, Park, Outside, Inside, I, Like, Dislike, That, Hungry, Thirsty, Tired, Cold
    }

    private Internals mInternal;
    private Bitmap mBitmap;
    private String mLibPath;

    public LeoIcon(Internals i) throws Exception {
        if (i == Internals.External)
            throw new Exception("Can't construct from that!");
        mInternal = i;
    }

    public LeoIcon(Bitmap b) {
        mInternal = Internals.External;
        mBitmap = b;
    }

    public LeoIcon(String s) {
        mInternal = Internals.Library;
        mLibPath = s;
    }

    public void Preload(Context ctx) {
        if (mInternal == Internals.Library && mLibPath.startsWith("A:")) {
            try {
                getBitmap(ctx);
            } catch (Exception e) {
            }
        }
    }

    // A primitive cache of bitmaps we've used from the arasaac jar, because
    // otherwise it's mightly slow.
    // TODO: It's very primitive, and has no size limit, so it could just
    // grow forever. On the other hand, we're not including search results,
    // just icons actually used in layouts.
    private static Map<String, Bitmap> mArasaacCache = new HashMap<String, Bitmap>();

    // Keep a reference to this jar to prevent having to re-open it all the
    // time.
    private static JarFile mArasaacJar = null;

    public Bitmap getBitmap(Context ctx) throws Exception {

        if (mInternal == Internals.External)
            return mBitmap;

        if (mInternal == Internals.Library) {
            if (!mLibPath.startsWith("A:")) {
                Log.d("Leo", "Unsupported library path '" + mLibPath + "'");
                return BitmapFactory.decodeResource(ctx.getResources(),
                        R.drawable.unknown);
            }
            String name = mLibPath.substring(2);
            Bitmap b;
            if (mArasaacCache.containsKey(name)) {
                b = mArasaacCache.get(name);
            } else {
                JarFile jf;
                if (mArasaacJar != null) {
                    jf = mArasaacJar;
                } else {
                    File jar = new File(ctx.getExternalFilesDir(null),
                            "arasaac_col_small.jar");
                    jf = new JarFile(jar);
                    mArasaacJar = jf;
                }
                JarEntry je = (JarEntry) jf.getEntry(name);
                if (je == null)
                    return BitmapFactory.decodeResource(ctx.getResources(),
                            R.drawable.unknown);
                InputStream is = jf.getInputStream(je);
                b = BitmapFactory.decodeStream(is);
                mArasaacCache.put(name, b);
                is.close();
                // TODO: Not closing jf here, because we're keeping the
                // reference now. But we should close it somewhere!
                // jf.close();
            }
            return b;
        }

        int id;
        switch (mInternal) {
        case Unknown:
            id = R.drawable.unknown;
            break;
        case Back:
            id = R.drawable.back;
            break;
        case Dog:
            id = R.drawable.dog;
            break;
        case Cat:
            id = R.drawable.cat;
            break;
        case Duck:
            id = R.drawable.duck;
            break;
        case Chicken:
            id = R.drawable.chicken;
            break;
        case Fish:
            id = R.drawable.fish;
            break;
        case Elephant:
            id = R.drawable.elephant;
            break;
        case Yes:
            id = R.drawable.yes;
            break;
        case No:
            id = R.drawable.no;
            break;
        case Good:
            id = R.drawable.good;
            break;
        case Bad:
            id = R.drawable.bad;
            break;
        case Animals:
            id = R.drawable.animals;
            break;
        case Want:
            id = R.drawable.want;
            break;
        case Go:
            id = R.drawable.go;
            break;
        case Eat:
            id = R.drawable.eat;
            break;
        case Biscuit:
            id = R.drawable.biscuit;
            break;
        case Biscuits:
            id = R.drawable.biscuits;
            break;
        case Apple:
            id = R.drawable.apple;
            break;
        case Home:
            id = R.drawable.home;
            break;
        case Park:
            id = R.drawable.park;
            break;
        case Outside:
            id = R.drawable.outside;
            break;
        case Inside:
            id = R.drawable.inside;
            break;
        case I:
            id = R.drawable.i;
            break;
        case Like:
            id = R.drawable.like;
            break;
        case Dislike:
            id = R.drawable.dislike;
            break;
        case That:
            id = R.drawable.that;
            break;
        case Hungry:
            id = R.drawable.hungry;
            break;
        case Thirsty:
            id = R.drawable.thirsty;
            break;
        case Tired:
            id = R.drawable.tired;
            break;
        case Cold:
            id = R.drawable.cold;
            break;
        default:
            throw new Exception("Invalid internal icon ID");
        }
        return BitmapFactory.decodeResource(ctx.getResources(), id);
    }

    public static LeoIcon Decode(String txt) throws Exception {
        if (txt.startsWith("$")) {
            return new LeoIcon(Internals.valueOf(txt.substring(1)));
        } else if (txt.startsWith("%")) {
            byte[] data = Base64.decode(txt.substring(1), Base64.DEFAULT);
            return new LeoIcon(BitmapFactory.decodeByteArray(data, 0,
                    data.length));
        } else if (txt.startsWith("#")) {
            return new LeoIcon(txt.substring(1));
        }
        throw new Exception("Invalid icon encoding");
    }

    public String Encoded() {
        switch (mInternal) {
        case External:
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mBitmap.compress(CompressFormat.PNG, 100, bos);
            byte[] data = bos.toByteArray();
            return "%" + Base64.encodeToString(data, Base64.DEFAULT);
        case Library:
            return "#" + mLibPath;
        default:
            return "$" + mInternal.toString();
        }
    }

}

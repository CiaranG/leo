package com.ciarang.leo;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

public class BoardLayout {

    public GroupItem RootItem;

    // The name of this layout. This will either be null (it's never been
    // saved) or a name, where xxxx.xml is the canonical name for the
    // XML file it came from.
    public String Name;

    // True if the layout has been modified since it was imported from the
    // original definition (as determined by the name field).
    public boolean Modified;

    public BoardLayout() {
        try {
            RootItem = new GroupItem(new LeoIcon(LeoIcon.Internals.Unknown),
                    "Root");
            Name = null;
            Modified = false;
        } catch (Exception e) {
            Log.d("Leo", "It can't get here, don't be silly");
        }
    }

    public void PreloadIcons(Context ctx) {
        ArrayList<GroupItem> searchgroups = new ArrayList<GroupItem>();        
        searchgroups.add(RootItem);
        while (!searchgroups.isEmpty()) {
            for (Item i : searchgroups.get(0).Items) {
                i.Icon.Preload(ctx);
                if (i.getClass() == GroupItem.class)
                    searchgroups.add((GroupItem) i);                
            }
            searchgroups.remove(0);
        }
    }
    
    public static BoardLayout fromXml(String xml) throws Exception {
        BoardLayout b = new BoardLayout();
        StringReader reader = new StringReader(xml);
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(reader);
        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
            case XmlPullParser.START_TAG:
                String name = parser.getName();
                if (name.equals("groupitem")) {
                    b.RootItem = GroupItem.FromXml(parser);
                } else if (name.equals("name")) {
                    parser.next();
                    b.Name = parser.getText();
                } else if (name.equals("modified")) {
                    b.Modified = true;
                }
                break;
            }
            eventType = parser.next();
        }
        return b;
    }

    public String toXml() throws IllegalArgumentException,
            IllegalStateException, IOException {
        XmlSerializer s = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        s.setOutput(writer);
        s.startDocument("UTF-8", true);
        s.startTag("", "boardlayout");
        s.attribute("", "version", String.valueOf(1));
        if (Name != null) {
            s.startTag("", "name");
            s.text(Name);
            s.endTag("", "name");
        }
        if (Modified) {
            s.startTag("", "modified");
            s.endTag("", "modified");
        }
        RootItem.ToXML(s);
        s.endTag("", "boardlayout");
        s.endDocument();
        return writer.toString();
    }

}

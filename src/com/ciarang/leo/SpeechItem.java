package com.ciarang.leo;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class SpeechItem extends Item {

    // ID of group to go to after this item, or null. When sentence building
    // reaches a SpeechItem with no Goto, that's the end of the sentence.
    // (or at least, the end of the current part of it - see also Item.After!)
    public String Goto;

    public SpeechItem(LeoIcon icon, String speech) {
        this(icon, speech, speech);
    }

    public SpeechItem(LeoIcon icon, String caption, String speech) {
        Caption = caption;
        Icon = icon;
        Speech = speech;
        Goto = null;
    }

    public void ToXML(XmlSerializer s) throws IllegalArgumentException,
            IllegalStateException, IOException {
        s.startTag("", "speechitem");
        CommonToXML(s);
        if (Goto != null) {
            s.startTag("", "goto");
            s.text(Goto);
            s.endTag("", "goto");
        }
        s.endTag("", "speechitem");
    }

    public static SpeechItem FromXml(XmlPullParser parser) throws Exception {
        SpeechItem g = new SpeechItem(new LeoIcon(LeoIcon.Internals.Unknown),
                "", "");
        int eventType = parser.next();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
            case XmlPullParser.START_TAG:
                String name = parser.getName();
                if (!handleCommonTag(name, g, parser)) {
                    if (name.equals("goto")) {
                        parser.next();
                        g.Goto = parser.getText();
                    }
                }
                break;
            case XmlPullParser.END_TAG:
                if (parser.getName().equals("speechitem"))
                    return g;
                break;
            }
            eventType = parser.next();
        }
        throw new Exception("Unexpected end of document");

    }

}
